CREATE TABLE users (
    email TEXT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    surname TEXT NOT NULL,
    password TEXT NOT NULL,
    gender TEXT NOT NULL,
    description TEXT NOT NULL,
    image TEXT NOT NULL
);

CREATE TABLE searches (
    _id INT PRIMARY KEY,
    email TEXT NOT NULL,
    search TEXT NOT NULL,

    FOREIGN KEY(email) REFERENCES users(email)
);

CREATE TABLE favorites (
    _id INT PRIMARY KEY,
    email TEXT NOT NULL,
    lat DOUBLE NOT NULL,
    lng DOUBLE NOT NULL,
    name TEXT NOT NULL,
    type TEXT NOT NULL,
    review TEXT NOT NULL,
    description TEXT NOT NULL,
    address TEXT NOT NULL,
    opening TEXT NOT NULL,
    closing TEXT NOT NULL
);

CREATE TABLE comments (
    _id INT PRIMARY KEY,
    email TEXT NOT NULL,
    lat DOUBLE NOT NULL,
    lng DOUBLE NOT NULL,
    comment TEXT NOT NULL
);