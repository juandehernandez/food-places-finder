package juan.lasalle.pprog2.practica.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by juandelacruz on 11/5/17.
 */

public class Restaurant {
    private LatLng coords;
    private String name;
    private String type;
    private String review;
    private String description;
    private String address;
    private String opening;
    private String closing;

    public Restaurant(LatLng coords, String name, String type, String review, String description, String address, String opening, String closing) {
        this.coords = coords;
        this.name = name;
        this.type = type;
        this.review = review;
        this.description = description;
        this.address = address;
        this.opening = opening;
        this.closing = closing;
    }

    public LatLng getCoords() {
        return coords;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getReview() {
        return review;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress() {
        return address;
    }

    public String getClosing() {
        return closing;
    }

    public String getOpening() {
        return opening;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setClosing(String closing) {
        this.closing = closing;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCoords(LatLng coords) {
        this.coords = coords;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setOpening(String opening) {
        this.opening = opening;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
