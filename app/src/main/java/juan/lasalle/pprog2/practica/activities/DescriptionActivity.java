package juan.lasalle.pprog2.practica.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.adapters.CommentsAdapter;
import juan.lasalle.pprog2.practica.model.Comment;
import juan.lasalle.pprog2.practica.repositories.UsersRepo;
import juan.lasalle.pprog2.practica.repositories.impl.UsersDB;
import juan.lasalle.pprog2.practica.utils.ListViewScrollUtility;

/**
 * Created by juandelacruz on 12/5/17.
 */

public class DescriptionActivity extends AppCompatActivity {
    private ListView listView;
    private String sessionUsername;
    private UsersRepo usersRepo;
    private CommentsAdapter commentsAdapter;
    private LatLng coords;
    private String name;
    private String type;
    private String address;
    private String review;
    private String description;
    private String opening;
    private String closing;

    public static int DESCRIPTION_ACTIVITY = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        coords = (LatLng) getIntent().getExtras().get("coords");

        //Database necessària per a insertar les búsquedes de l'usuari i recuperar-les també
        usersRepo = new UsersDB(getApplicationContext());

        //Recuperem la sessió de l'usuari per a saber a qui insertar les búsquedes
        SharedPreferences settings = getSharedPreferences(MainActivity.SESSION_PREFERENCES, 0);
        sessionUsername = settings.getString("username", null);

        listView = (ListView) findViewById(R.id.listview);

        //Recuperem els comentaris ja del restaurant
        List<Comment> commentsList = new ArrayList<>();
        commentsList.addAll(usersRepo.getCommentsFromRestaurant(coords));

        //Generem l'adaptador per als comentaris
        commentsAdapter = new CommentsAdapter(getApplicationContext(), commentsList);

        // Vinculamos el adapter a la ListView.
        listView.setAdapter(commentsAdapter);

        ListViewScrollUtility.setListViewHeightBasedOnChildren(listView);

        name = getIntent().getExtras().get("name").toString();
        review = getIntent().getExtras().get("review").toString();
        description = getIntent().getExtras().get("description").toString();
        type = getIntent().getExtras().get("type").toString();
        opening = getIntent().getExtras().get("opening").toString();
        closing = getIntent().getExtras().get("closing").toString();
        address = getIntent().getExtras().get("address").toString();

        TextView nameTextView = (TextView) findViewById(R.id.name);
        RatingBar reviewRatingBar = (RatingBar) findViewById(R.id.review);
        TextView descriptionTextView = (TextView) findViewById(R.id.description);
        ImageView imageView = (ImageView) findViewById(R.id.image);

        //Obtenemos el tipo de lugar de comida del intent y ponemos la imagen concreta
        switch (getIntent().getExtras().get("type").toString()) {
            case "Hamburgueseria":
                imageView.setImageResource(R.drawable.hamburgueseria_local);
                break;
            case "Oriental":
                imageView.setImageResource(R.drawable.oriental_local);
                break;
            case "Mejicano":
                imageView.setImageResource(R.drawable.mejicano_local);
                break;
            case "Restaurante":
                imageView.setImageResource(R.drawable.restaurante_local);
                break;
            case "Nepali":
                imageView.setImageResource(R.drawable.nepali_local);
                break;
            case "Take Away":
                imageView.setImageResource(R.drawable.takeaway_local);
                break;
            case "Asador":
                imageView.setImageResource(R.drawable.asador_local);
                break;
            case "Frankfurt":
                imageView.setImageResource(R.drawable.hotdog_local);
                break;
        }
        nameTextView.setText(name);
        reviewRatingBar.setRating(Float.parseFloat(review));
        descriptionTextView.setText(description);

        createToolbar();
    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Este metodo se llamara una vez durante la creacion de la Activity
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Este metodo se llamara cada vez que se pulse en algun Item de la Action Bar
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_profile_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, ProfileActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, ProfileActivity.PROFILE_ACTIVITY);
                break;
            case R.id.menu_favorites_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, FavoritesActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, FavoritesActivity.FAVORITES_ACTIVITY);
            default:

        }
        return true;
    }

    public void addToFavoritesOnClick(View view) {
        boolean existsFavorite = usersRepo.existsFavorite(sessionUsername, coords.latitude, coords.longitude);

        if (!existsFavorite) {
            usersRepo.addFavorite(sessionUsername, coords.latitude, coords.longitude, name, type, review, description, address, opening, closing);
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.restaurant_added_favorites_alert), Snackbar.LENGTH_LONG)
                    .show();
        } else {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.restaurant_exists_favorites_alert), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void openLocationOnClick(View view) {
        // Creamos un intent explícito que llame a la segunda activitidad.
        Intent intent = new Intent(this, LocationActivity.class);
        intent.putExtra("coords", coords);
        // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
        // para poder reconocer el valor de retorno.
        startActivityForResult(intent, LocationActivity.LOCATION_ACTIVITY);
    }

    public void addCommentOnClick(View view) {
        TextInputEditText commentEditText = (TextInputEditText) findViewById(R.id.comment_edittext);
        usersRepo.addComment(sessionUsername, coords.latitude, coords.longitude, commentEditText.getText().toString());
        commentsAdapter.clear();
        commentsAdapter.updateData(usersRepo.getCommentsFromRestaurant(coords));
        commentsAdapter.notifyDataSetChanged();

        ListViewScrollUtility.setListViewHeightBasedOnChildren(listView);
    }
}
