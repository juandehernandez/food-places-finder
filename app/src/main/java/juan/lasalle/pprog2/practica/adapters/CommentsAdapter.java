package juan.lasalle.pprog2.practica.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.model.Comment;
import juan.lasalle.pprog2.practica.model.Restaurant;

/**
 * Created by juandelacruz on 26/5/17.
 */

public class CommentsAdapter extends ArrayAdapter<Comment> {
    private List<Comment> comments;

    public CommentsAdapter(Context context, List<Comment> comments) {
        super(context, 0, comments);
        this.comments = comments;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Comment comment = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_comments_layout, parent, false);
        }
        // Lookup view for data population
        TextView userTextView = (TextView) convertView.findViewById(R.id.listview_cell_user) ;
        TextView commentTextView = (TextView) convertView.findViewById(R.id.listview_cell_comment);

        // Populate the data into the template view using the data object
        userTextView.setText(comment.getUser());
        commentTextView.setText(comment.getText());
        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    public void updateData(List<Comment> list){
        comments.clear();
        comments.addAll(list);
    }
}
