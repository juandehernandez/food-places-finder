package juan.lasalle.pprog2.practica.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.adapters.RecentSearchesAdapter;
import juan.lasalle.pprog2.practica.adapters.RestaurantListViewAdapter;
import juan.lasalle.pprog2.practica.model.Restaurant;
import juan.lasalle.pprog2.practica.model.User;
import juan.lasalle.pprog2.practica.repositories.RestaurantWebServiceRepo;
import juan.lasalle.pprog2.practica.repositories.UsersRepo;
import juan.lasalle.pprog2.practica.repositories.impl.RestaurantWebService;
import juan.lasalle.pprog2.practica.repositories.impl.UsersDB;
import juan.lasalle.pprog2.practica.service.LocationService;

import static android.R.id.list;
import static java.lang.StrictMath.abs;

/**
 * Created by juandelacruz on 21/4/17.
 */

public class SearchActivity extends AppCompatActivity{
    private UsersRepo usersRepo;
    private String sessionUsername;
    private EditText searchEditText;
    private TextView radiusTextView;
    private RecentSearchesAdapter searchesAdapter;
    private List<String> searchesList;

    public static final int SEARCH_ACTIVITY = 3;
    public static final String TAG_METHOD = "method";
    public static final String TAG_CRITERIA = "criteria";
    public static final String TAG_GEOLOCATION = "geolocation";
    public static final String TAG_DISTANCE = "distance";
    public static final String TAG_FAVORITES = "favorites";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //Intent para recibir por si venimos del registro o de resultados si no se encontró nada en la búsqueda
        Intent intent = getIntent();

        //Extras si es que tenemos
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey(RegisterActivity.TAG_MESSAGE)) {
                String message = extras.getString(RegisterActivity.TAG_MESSAGE);

                if (message != null) {
                    Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
                            .show();
                }
            } else if (extras.containsKey(ResultsActivity.TAG_MESSAGE)) {
                String message = extras.getString(ResultsActivity.TAG_MESSAGE);

                if (message != null) {
                    Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        }

        ListView listView = (ListView) findViewById(R.id.listview);

        //Database necessària per a insertar les búsquedes de l'usuari i recuperar-les també
        usersRepo = new UsersDB(getApplicationContext());

        //Recuperem la sessió de l'usuari per a saber a qui insertar les búsquedes
        SharedPreferences settings = getSharedPreferences(MainActivity.SESSION_PREFERENCES, 0);
        sessionUsername = settings.getString("username", null);

        //Recuperem les búsquedes ja de l'usuari
        searchesList = new ArrayList<>();
        searchesList.addAll(usersRepo.getSearchesFromUser(sessionUsername));

        //Generem l'adaptador per a les búsquedes
        searchesAdapter = new RecentSearchesAdapter(getApplicationContext(), searchesList);

        // Vinculamos el adapter a la ListView.
        listView.setAdapter(searchesAdapter);

        // Vinculamos el adapter (que implementa un OnItemClickListener) como Listener de cada
        // uno de los items de la ListView.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Creamos un intent explícito que llame a la segunda activitidad.
                Intent intent = new Intent(getApplicationContext(), ResultsActivity.class);
                intent.putExtra(TAG_METHOD, "criteria");
                intent.putExtra(TAG_CRITERIA, searchesList.get(position));
                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, ResultsActivity.RESULTS_ACTIVITY);
            }
        });

        listView.setFastScrollAlwaysVisible(true);

        searchEditText = (EditText) findViewById(R.id.search_edittext);

        searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && searchEditText.getText().toString().equals(getString(R.string.search_edittext)))
                    searchEditText.setText("");
                else
                    if(searchEditText.getText().toString().isEmpty()) {
                        searchEditText.setText(R.string.search_edittext);
                    }
            }
        });


        SeekBar radiusSeekBar = (SeekBar) findViewById(R.id.search_radius_seekBar);
        radiusTextView = (TextView) findViewById(R.id.radius_textview);

        radiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                radiusTextView.setText(String.valueOf(progress) + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        createToolbar();

        //Se comprueba si el usuario tiene permisos activados a la aplicación
        // para mostrar su ubicación
        checkLocationPermission();

        LocationService.getInstance(getApplicationContext());

    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationService locationService = LocationService.getInstance(getApplicationContext());
        locationService.registerListeners(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocationService locationService = LocationService.getInstance(getApplicationContext());
        locationService.unregisterListeners();
    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Este metodo se llamara una vez durante la creacion de la Activity
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Este metodo se llamara cada vez que se pulse en algun Item de la Action Bar
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_profile_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, ProfileActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, ProfileActivity.PROFILE_ACTIVITY);
                break;
            case R.id.menu_favorites_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, FavoritesActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, FavoritesActivity.FAVORITES_ACTIVITY);
            default:

        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    public void searchButtonOnClick(View view) {
        // Creamos un intent explícito que llame a la segunda activitidad.
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra(TAG_METHOD,  "criteria");
        intent.putExtra(TAG_CRITERIA, searchEditText.getText().toString());
        if (!searchEditText.getText().toString().equals(getString(R.string.search_edittext))) {
            usersRepo.addSearch(sessionUsername, searchEditText.getText().toString());
        }
        searchesAdapter.clear();
        searchesAdapter.updateData(usersRepo.getSearchesFromUser(sessionUsername));
        searchesAdapter.notifyDataSetChanged();
        // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
        // para poder reconocer el valor de retorno.
        startActivityForResult(intent, ResultsActivity.RESULTS_ACTIVITY);
    }

    public void geolocationSearchButtonOnClick(View view) {
        Location location = LocationService.getInstance(getApplicationContext()).getLocation();
        if (location != null) {
            // Creamos un intent explícito que llame a la segunda activitidad.
            Intent intent = new Intent(this, ResultsActivity.class);
            intent.putExtra(TAG_METHOD,  "geolocation");
            intent.putExtra(TAG_GEOLOCATION, location);
            SeekBar distanceSeekBar = (SeekBar) findViewById(R.id.search_radius_seekBar);
            int distance = distanceSeekBar.getProgress();
            intent.putExtra(TAG_DISTANCE, distance);
            // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
            // para poder reconocer el valor de retorno.
            startActivityForResult(intent, ResultsActivity.RESULTS_ACTIVITY);
        } else {
            //Location is null
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.location_null), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void clearSearchTextButtonOnClick(View view) {
        searchEditText = (EditText) findViewById(R.id.search_edittext);
        searchEditText.setText(getString(R.string.search_edittext));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LocationService.MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocationService locationService = LocationService.getInstance(getApplicationContext());
                    locationService.registerListeners(this);
                }
                break;
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission. ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission. ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.permission_title)
                        .setMessage(R.string.permission_message)
                        .setPositiveButton(R.string.permission_ok_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(SearchActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission. ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

}
