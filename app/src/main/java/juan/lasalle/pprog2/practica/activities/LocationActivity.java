package juan.lasalle.pprog2.practica.activities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.service.*;
/**
 * Created by juandelacruz on 27/5/17.
 */

public class LocationActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final int LOCATION_ACTIVITY = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        createToolbar();

        LocationService.getInstance(getApplicationContext());

        MapFragment mapFragment = MapFragment.newInstance();

        //Cargamos el fragmento del mapa
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.map, mapFragment);
        transaction.commit();

        mapFragment.getMapAsync(this);

    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Este metodo se llamara una vez durante la creacion de la Activity
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Este metodo se llamara cada vez que se pulse en algun Item de la Action Bar
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_profile_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, ProfileActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, ProfileActivity.PROFILE_ACTIVITY);
                break;
            case R.id.menu_favorites_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, FavoritesActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, FavoritesActivity.FAVORITES_ACTIVITY);
            default:

        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationService locationService = LocationService.getInstance(getApplicationContext());
        locationService.registerListeners(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocationService locationService = LocationService.getInstance(getApplicationContext());
        locationService.unregisterListeners();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LocationService.MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocationService.getInstance(this).registerListeners(this);
                }
                break;
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        //Recibimos las coordenadas del intent del DescriptionActivity y acercamos cámara a la posición
        //del restaurante añadiéndole también un marcador
        final LatLng restaurantLocation = (LatLng) getIntent().getExtras().get("coords");
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(restaurantLocation, 15));
        googleMap.addMarker(new MarkerOptions().position(restaurantLocation)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        //Cuando el mapa está listo activamos nuestra localización
        googleMap.setMyLocationEnabled(true);
        //Ponemos un listener al clicar el botón de nuestra localización
        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //Coge nuestra localización de gps gracias al LocationService
                Location location = LocationService.getInstance(getApplicationContext()).getLocation();
                final LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                //Acercamos la cámara hacia donde nos encontramos
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
                return false;
            }
        });

    }
}
