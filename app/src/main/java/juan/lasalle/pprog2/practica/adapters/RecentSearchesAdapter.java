package juan.lasalle.pprog2.practica.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.model.Restaurant;

/**
 * Created by juandelacruz on 23/5/17.
 */

public class RecentSearchesAdapter extends ArrayAdapter<String> {
    private List<String> searches;

    public RecentSearchesAdapter(Context context, List<String> searches) {
        super(context, 0, searches);
        this.searches = searches;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        String search = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_searches_layout, parent, false);
        }
        // Lookup view for data population
        TextView searchTextView = (TextView) convertView.findViewById(R.id.listview_cell_search);

        // Populate the data into the template view using the data object
        searchTextView.setText(search);
        // Return the completed view to render on screen
        return convertView;
    }

    public void updateData(List<String> list){
        searches.clear();
        searches.addAll(list);
    }
}
