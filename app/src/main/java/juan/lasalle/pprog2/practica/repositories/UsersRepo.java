package juan.lasalle.pprog2.practica.repositories;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import juan.lasalle.pprog2.practica.model.Comment;
import juan.lasalle.pprog2.practica.model.Restaurant;
import juan.lasalle.pprog2.practica.model.User;

/**
 * Created by juandelacruz on 18/4/17.
 */

public interface UsersRepo {
    void addUser(User p);
    void addSearch(String email, String search);
    void addFavorite(String email, Double lat, Double lng, String name, String type, String review, String description,
                     String address, String opening, String closing);
    void addComment(String email, Double lat, Double lng, String comment);
    boolean existsFavorite(String name, Double lat, Double lng);
    boolean succesfulLogin(String email, String password);
    boolean registeredUser(String email);
    List<User> getAllUsers();
    List<String> getSearchesFromUser(String email);
    List<Restaurant> getFavoritesFromUser(String email);
    List<Comment> getCommentsFromRestaurant(LatLng restaurant);
    void updatePicture(String email, byte[] image);
    void updateName(String email, String newName);
    void updateSurname(String email, String newSurname);
    void updateDescription(String email, String newDescription);
    void updateGender(String email, String newGender);
    byte[] getImage(String email);
    User getUserProfile(String email);

}