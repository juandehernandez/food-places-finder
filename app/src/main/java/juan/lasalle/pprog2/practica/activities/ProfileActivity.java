package juan.lasalle.pprog2.practica.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.model.User;
import juan.lasalle.pprog2.practica.repositories.UsersRepo;
import juan.lasalle.pprog2.practica.repositories.impl.UsersDB;
import juan.lasalle.pprog2.practica.utils.DbBitmapUtility;

/**
 * Created by juandelacruz on 13/5/17.
 */

public class ProfileActivity extends AppCompatActivity {
    private UsersRepo usersRepo;
    private ImageView imageView;
    private String sessionUsername;
    private boolean profileEditableStatus = false;
    private static final int TAKE_PICTURE = 8;
    private static final int PICK_IMAGE = 9;
    public static final int PROFILE_ACTIVITY = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        usersRepo = new UsersDB(getApplicationContext());
        SharedPreferences settings = getSharedPreferences(MainActivity.SESSION_PREFERENCES, 0);
        sessionUsername = settings.getString("username", null);
        Log.d("pape", sessionUsername);

        byte[] userImage = usersRepo.getImage(sessionUsername);
        imageView = (ImageView) findViewById(R.id.draw_picture);
        imageView.setImageBitmap(DbBitmapUtility.getImage(userImage));
        imageView.getLayoutParams().height = 185;
        imageView.getLayoutParams().width = 185;

        //Ponemos el perfil editable para que se pongan los datos y aprovechar el código dentro del método privado
        profileEditable(true);
        //Ahora lo deshabilitamos
        profileEditable(false);

        createToolbar();
    }

    private void profileEditable(boolean editable) {
        profileEditableStatus = !profileEditableStatus;

        EditText nameEditText = (EditText) findViewById(R.id.name_edittext);
        EditText surnameEditText = (EditText) findViewById(R.id.surname_edittext);
        EditText descriptionEditText = (EditText) findViewById(R.id.description_edittext);
        RadioButton maleRadioButton = (RadioButton) findViewById(R.id.male_radiobutton);
        RadioButton femaleRadioButton = (RadioButton) findViewById(R.id.female_radiobutton);
        Button takePictureButton = (Button) findViewById(R.id.take_picture_button);
        Button choosePictureButton = (Button) findViewById(R.id.choose_picture_button);
        Button saveProfileButton = (Button) findViewById(R.id.save_profile_button);

        nameEditText.setEnabled(editable);
        surnameEditText.setEnabled(editable);
        descriptionEditText.setEnabled(editable);
        maleRadioButton.setEnabled(editable);
        femaleRadioButton.setEnabled(editable);
        takePictureButton.setEnabled(editable);
        choosePictureButton.setEnabled(editable);
        saveProfileButton.setEnabled(editable);

        if (editable) {
            User user = usersRepo.getUserProfile(sessionUsername);
            Log.d("pepaaa", user.getEmail());
            Log.d("pepaaa", user.getName());
            nameEditText.setText(user.getName());
            surnameEditText.setText(user.getSurname());
            descriptionEditText.setText(user.getDescription());
            switch(user.getGender()) {
                case "Sexo masculino":
                case "Male":
                    femaleRadioButton.setChecked(false);
                    maleRadioButton.setChecked(true);
                    break;
                case "Sexo femenino":
                case "Female":
                    maleRadioButton.setChecked(false);
                    femaleRadioButton.setChecked(true);
                    break;
            }
        }
    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Este metodo se llamara una vez durante la creacion de la Activity
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_profile, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Este metodo se llamara cada vez que se pulse en algun Item de la Action Bar
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_edit_action_button:
                profileEditable(!profileEditableStatus);
                break;
            case R.id.menu_favorites_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, FavoritesActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, FavoritesActivity.FAVORITES_ACTIVITY);
                break;
            default:

        }
        return true;
    }

    public void takeAPictureOnClick (View view) {
        // Creamos un intent implícito que llame a alguna aplicación capaz de tomar fotos.
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
        // para poder reconocer el valor de retorno.
        startActivityForResult(intent, TAKE_PICTURE);
    }

    public void chooseAPictureOnClick(View view) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    public void saveProfileOnClick(View view) {
        EditText nameEditText = (EditText) findViewById(R.id.name_edittext);
        EditText surnameEditText = (EditText) findViewById(R.id.surname_edittext);
        EditText descriptionEditText = (EditText) findViewById(R.id.description_edittext);
        RadioGroup gender = (RadioGroup) findViewById(R.id.gender_radiogroup);

        String name = nameEditText.getText().toString();
        String surname = surnameEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        String selectedGender = ((RadioButton)findViewById(gender.getCheckedRadioButtonId())).getText().toString();
        //Si la aplicación está en español entonces sale en otro idioma al inglés
        //Lo pasamos al ingles para la base de datos
        if (selectedGender.equals("Sexo masculino")) {
            selectedGender = "Male";
        } else if (selectedGender.equals("Sexo femenino")) {
            selectedGender = "Female";
        }


        //Obtenemos los datos del usuario para ver si ha modificado algun dato
        User user = usersRepo.getUserProfile(sessionUsername);

        if (!name.equals("") && !name.equals(user.getName())) {
            usersRepo.updateName(sessionUsername, name);
        }
        if (!surname.equals("") && !surname.equals(user.getSurname())) {
            usersRepo.updateSurname(sessionUsername, surname);
        }
        if (!description.equals("") && !description.equals(user.getDescription())) {
            usersRepo.updateDescription(sessionUsername, description);
        }
        if (!selectedGender.equals(user.getGender())) {
            usersRepo.updateGender(sessionUsername, selectedGender);
        }

        profileEditable(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Filtramos resultado si es de hacer la foto
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                Bitmap image = (Bitmap) bundle.get("data");

                if (imageView != null) {
                    imageView.setImageBitmap(image);
                    byte[] imageBytes = DbBitmapUtility.getBytes(image);
                    usersRepo.updatePicture(sessionUsername, imageBytes);
                }
            }
        }
    }
}
