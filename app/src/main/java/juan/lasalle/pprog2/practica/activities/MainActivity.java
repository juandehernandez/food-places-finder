package juan.lasalle.pprog2.practica.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.model.User;
import juan.lasalle.pprog2.practica.repositories.UsersRepo;
import juan.lasalle.pprog2.practica.repositories.impl.UsersDB;

public class MainActivity extends AppCompatActivity {
    private UsersRepo usersRepo;
    public List<User> list;

    public static final int MAIN_ACTIVITY = 1;
    public static final String SESSION_PREFERENCES = "SESSION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usersRepo = new UsersDB(getApplicationContext());

        list = new ArrayList<>();

        //Listener de keyboard per al edittext. Així es pot fer login amb enter al camp password
        TextInputEditText tiePassword = (TextInputEditText) findViewById(R.id.password_edittext);
        tiePassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            loginButtonOnClick(findViewById(android.R.id.content));
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    public void loginButtonOnClick(View view) {
        TextInputEditText tieEmail = (TextInputEditText) findViewById(R.id.username_edittext);
        TextInputEditText tiePassword = (TextInputEditText) findViewById(R.id.password_edittext);
        String email = tieEmail.getText().toString();
        String password = tiePassword.getText().toString();

        if (usersRepo.succesfulLogin(email, password)) {
            // Creamos un intent explícito que llame a la segunda activitidad.
            Intent intent = new Intent(this, SearchActivity.class);

            // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
            // para poder reconocer el valor de retorno.
            startActivityForResult(intent, SearchActivity.SEARCH_ACTIVITY);

            SharedPreferences settings = getSharedPreferences(SESSION_PREFERENCES, 0);
            SharedPreferences.Editor editor = settings.edit();

            editor.putString("username", email);
            editor.commit();
        } else {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.login_unsuccessful_alert), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public void sendToRegisterActivity(View view) {
        // Creamos un intent explícito que llame a la segunda activitidad.
        Intent intent = new Intent(this, RegisterActivity.class);

        // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
        // para poder reconocer el valor de retorno.
        startActivityForResult(intent, RegisterActivity.REGISTER_ACTIVITY);
    }

}
