package juan.lasalle.pprog2.practica.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import juan.lasalle.pprog2.practica.model.Restaurant;

import static juan.lasalle.pprog2.practica.activities.ResultsActivity.inputFormat;

/**
 * Created by juandelacruz on 27/5/17.
 */

public class OpenRestaurantsUtility {
    private static SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);

    public static List<Restaurant> getOpenRestaurants(List<Restaurant> restaurantList) {
        List<Restaurant> restaurantListOpen = new ArrayList<>();

        //Este bucle busca en la lista de todos los restaurantes obtenida en este mismo método
        //los que estan abiertos en este momento.
        for (int i = 0; i < restaurantList.size(); i++) {
            String opening = restaurantList.get(i).getOpening();
            String closing = restaurantList.get(i).getClosing();

            //Hora actual
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String str = sdf.format(new Date());

            Date date = parseDate(str);
            Date dateCompareOne = parseDate(opening);
            Date dateCompareTwo = parseDate(closing);
            //Si la hora actual está entre la hora de apertura y cierre del restaurante
            //se añade a la lista de restaurantes abiertos
            if (dateCompareOne.before(date) && dateCompareTwo.after(date)) {
                restaurantListOpen.add(restaurantList.get(i));
            }
        }

        return restaurantListOpen;
    }

    private static Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }
}
