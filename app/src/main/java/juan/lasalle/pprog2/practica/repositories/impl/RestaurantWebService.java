package juan.lasalle.pprog2.practica.repositories.impl;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import juan.lasalle.pprog2.practica.model.Restaurant;
import juan.lasalle.pprog2.practica.repositories.RestaurantWebServiceRepo;
import juan.lasalle.pprog2.practica.utils.HttpRequestHelper;

/**
 * Created by juandelacruz on 11/5/17.
 */

public class RestaurantWebService implements RestaurantWebServiceRepo{
    private static final String OMDBAPI_URI_BASE = "http://testapi-pprog2.azurewebsites.net/api/locations.php?method=getLocations";
    private static final String NAME_TAG = "name";
    private static final String TYPE_TAG = "type";
    private static final String REVIEW_TAG = "review";
    private static final String DESCRIPTION_TAG = "description";
    private static final String ADDRESS_TAG = "address";
    private static final String OPENING_TAG = "opening";
    private static final String CLOSING_TAG = "closing";
    private static final String LATITUDE_TAG = "lat";
    private static final String LONGITUDE_TAG = "lng";
    private static final String LOCATION_TAG = "location";


    @Override
    public List<Restaurant> search(String criteria) {
        // criteria es el nombre del restaurante

        ArrayList<Restaurant> list = new ArrayList<>();
        HttpRequestHelper requestHelper = HttpRequestHelper.getInstance();
        JSONArray json = requestHelper.doHttpRequest(OMDBAPI_URI_BASE, "GET");
        try {

            JSONObject restaurantObject;
            for (int i = 0; i < json.length(); i++) {

                restaurantObject = json.getJSONObject(i);
                JSONObject jsonLocations = restaurantObject.getJSONObject(LOCATION_TAG);
                Double lat = jsonLocations.getDouble(LATITUDE_TAG);
                Double lng = jsonLocations.getDouble(LONGITUDE_TAG);
                LatLng coord = new LatLng(lat, lng);
                String name = restaurantObject.get(NAME_TAG).toString();
                String address = restaurantObject.get(ADDRESS_TAG).toString();
                String type = restaurantObject.get(TYPE_TAG).toString();
                if (name.toLowerCase().matches(".*\\b" + criteria.toLowerCase() + "\\b.*")
                        || address.toLowerCase().matches(".*\\b" + criteria.toLowerCase() + "\\b.*")
                        || type.toLowerCase().matches(".*\\b" + criteria.toLowerCase() + "\\b.*")) {
                    String review = restaurantObject.get(REVIEW_TAG).toString();
                    String description = restaurantObject.get(DESCRIPTION_TAG).toString();
                    String opening = restaurantObject.get(OPENING_TAG).toString();
                    String closing = restaurantObject.get(CLOSING_TAG).toString();

                    Restaurant restaurant = new Restaurant(coord, name, type, review, description, address, opening, closing);

                    list.add(restaurant);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    public List<Restaurant> searchGeolocation(String latitude, String longitude, String distance) {
        //latitud y longitud de entrada para comparar distancia con los obtenidos en json
        //los convertimos en double para la comparacion de distancia
        Double userLatitude = Double.parseDouble(latitude);
        Double userLongitude = Double.parseDouble(longitude);
        int radius = Integer.parseInt(distance);
        LatLng userCoord = new LatLng(userLatitude, userLongitude);

        //Log.d("user latitude", Double.toString(userLatitude));
        //Log.d("user longitude", Double.toString(userLongitude));

        ArrayList<Restaurant> list = new ArrayList<>();
        HttpRequestHelper requestHelper = HttpRequestHelper.getInstance();
        JSONArray json = requestHelper.doHttpRequest(OMDBAPI_URI_BASE, "GET");
        //Log.d(MoviesWebService.class.getName(), json.toString());
        try {
            //Log.d(MoviesWebService.class.getName(), OMDBAPI_URI_BASE + searchParameter);

            JSONObject restaurantObject;
            for (int i = 0; i < json.length(); i++) {
                //Log.d("Json", json.getJSONObject(i).get("name").toString());

                restaurantObject = json.getJSONObject(i);
                JSONObject jsonLocations = restaurantObject.getJSONObject(LOCATION_TAG);
                Double lat = jsonLocations.getDouble(LATITUDE_TAG);
                Double lng = jsonLocations.getDouble(LONGITUDE_TAG);
                LatLng coord = new LatLng(lat, lng);
                //Log.d("restaurant lat", Double.toString(lat));
                //Log.d("restaurant lng", Double.toString(lng));
                //float[] result = new float[5];
                //Location.distanceBetween(userLatitude, userLongitude, lat, lng, result);
                //Log.d("result", Float.toString(result[0]));
                if (CalculationByDistance(userCoord, coord) <= radius) {
                    String name = restaurantObject.get(NAME_TAG).toString();
                    String type = restaurantObject.get(TYPE_TAG).toString();
                    String review = restaurantObject.get(REVIEW_TAG).toString();
                    String description = restaurantObject.get(DESCRIPTION_TAG).toString();
                    String address = restaurantObject.get(ADDRESS_TAG).toString();
                    String opening = restaurantObject.get(OPENING_TAG).toString();
                    String closing = restaurantObject.get(CLOSING_TAG).toString();

                    Restaurant restaurant = new Restaurant(coord, name, type, review, description, address, opening, closing);

                    list.add(restaurant);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.d("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }
}
