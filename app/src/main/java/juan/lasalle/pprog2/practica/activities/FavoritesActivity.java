package juan.lasalle.pprog2.practica.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.adapters.CommentsAdapter;
import juan.lasalle.pprog2.practica.adapters.RestaurantListViewAdapter;
import juan.lasalle.pprog2.practica.adapters.TabAdapter;
import juan.lasalle.pprog2.practica.fragments.AllListFragment;
import juan.lasalle.pprog2.practica.fragments.OpenListFragment;
import juan.lasalle.pprog2.practica.model.Comment;
import juan.lasalle.pprog2.practica.model.Restaurant;
import juan.lasalle.pprog2.practica.repositories.RestaurantWebServiceRepo;
import juan.lasalle.pprog2.practica.repositories.UsersRepo;
import juan.lasalle.pprog2.practica.repositories.impl.RestaurantWebService;
import juan.lasalle.pprog2.practica.repositories.impl.UsersDB;
import juan.lasalle.pprog2.practica.utils.OpenRestaurantsUtility;

/**
 * Created by juandelacruz on 25/5/17.
 */

public class FavoritesActivity extends AppCompatActivity{
    private AllListFragment allListFragment;
    private OpenListFragment openListFragment;
    private UsersRepo usersRepo;
    private String sessionUsername;

    public ArrayList<Restaurant> restaurantList;
    public ArrayList<Restaurant> restaurantListOpen;
    public static final int FAVORITES_ACTIVITY = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        //Creamos nuevos objetos para los fragmentos de las listas de restaurantes
        //así les pasamos la lista de restaurantes con su método dataChanged
        allListFragment = new AllListFragment();
        openListFragment = new OpenListFragment();

        //Generemamos repositorio de la base de datos para obtener los favoritos de un usuario
        usersRepo = new UsersDB(getApplicationContext());

        //Recuperamos sesión actual del usuario
        SharedPreferences settings = getSharedPreferences(MainActivity.SESSION_PREFERENCES, 0);
        sessionUsername = settings.getString("username", null);

        //Detectar el cambio de tab, de esta manera modificamos spinner de los filtros
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equals(getString(R.string.all_tab))) {
                    allListFragment.tabSelected();
                } else if (tab.getText().toString().equals(getString(R.string.only_open_tab))) {
                    openListFragment.tabSelected();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        new AsyncRequest().execute();
    }

    private class AsyncRequest extends AsyncTask<String, Void, List<Restaurant>> {

        private Context context;
        private ProgressDialog progressDialog;

        protected AsyncRequest() {
            this.context = FavoritesActivity.this;
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage(getString(R.string.wait_message));
            progressDialog.show();
        }

        @Override
        protected List<Restaurant> doInBackground(String... params) {
            return usersRepo.getFavoritesFromUser(sessionUsername);
        }

        @Override
        protected void onPostExecute(final List<Restaurant> list) {
            super.onPostExecute(list);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            createToolbar();
            createTabs();

            List<Restaurant> openRestaurants = OpenRestaurantsUtility.getOpenRestaurants(list);

            //Ahora le pasaremos a cada fragmento los tipos de lugares de comida (Mejicano, Asador...)
            //para filtros del spinner

            //Generamos arraylist para obtener los tipos diferentes de restaurantes (total de restaurantes)
            List<String> restaurantTypesAll = new ArrayList<>();
            List<String> restaurantTypesOpen = new ArrayList<>();
            restaurantTypesAll.add(0, getString(R.string.default_text_spinner));
            restaurantTypesOpen.add(0, getString(R.string.default_text_spinner));
            //Bucle todos los restaurantes
            for (int i = 0; i < list.size(); i++) {
                if (!restaurantTypesAll.contains(list.get(i).getType())) {
                    restaurantTypesAll.add(list.get(i).getType());
                }
            }
            //Bucle restaurantes abiertos
            for (int i = 0; i < openRestaurants.size(); i++) {
                if (!restaurantTypesOpen.contains(openRestaurants.get(i).getType())) {
                    restaurantTypesOpen.add(openRestaurants.get(i).getType());
                }
            }

            //Le ponemos también el tipo TODOS
            // así se puede volver a mostrar todos los restaurantes para el spinner
            restaurantTypesAll.add(getString(R.string.spinner_all));
            restaurantTypesOpen.add(getString(R.string.spinner_all));

            //En el arrayadapter se necesita un array de String por eso convertimos el arraylist a array
            String[] spinnerTypesAll = new String[restaurantTypesAll.size()];
            spinnerTypesAll = restaurantTypesAll.toArray(spinnerTypesAll);

            //y ahora el de restaurantes abiertos
            String[] spinnerTypesOpen = new String[restaurantTypesOpen.size()];
            spinnerTypesOpen = restaurantTypesOpen.toArray(spinnerTypesOpen);

            allListFragment.setSpinnerTypes(spinnerTypesAll);
            openListFragment.setSpinnerTypes(spinnerTypesOpen);

            //Finalmente pasamos las listas de restaurantes a cada fragmento
            allListFragment.dataChanged(list);
            openListFragment.dataChanged(openRestaurants);

        }
    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    private void createTabs() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);

        ArrayList<TabAdapter.TabEntry> entries = new ArrayList<>();
        entries.add(new TabAdapter.TabEntry(allListFragment, getString(R.string.all_tab)));
        entries.add(new TabAdapter.TabEntry(openListFragment, getString(R.string.only_open_tab)));

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager(), entries);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Este metodo se llamara una vez durante la creacion de la Activity
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_favorites, menu);

        return true;
    }
}
