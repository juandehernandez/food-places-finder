package juan.lasalle.pprog2.practica.repositories;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

import juan.lasalle.pprog2.practica.model.Restaurant;

/**
 * Created by juandelacruz on 11/5/17.
 */

public interface RestaurantWebServiceRepo {
    List<Restaurant> search(String criteria);
    List<Restaurant> searchGeolocation(String latitude, String longitude, String distance);
}
