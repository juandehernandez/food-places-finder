package juan.lasalle.pprog2.practica.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.model.Restaurant;

/**
 * Created by juandelacruz on 11/5/17.
 */

public class RestaurantListViewAdapter extends ArrayAdapter<Restaurant> {
    private List<Restaurant> restaurants;

    public RestaurantListViewAdapter(Context context, List<Restaurant> restaurants) {
        super(context, 0, restaurants);
        this.restaurants = restaurants;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Restaurant restaurant = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_cell_layout, parent, false);
        }
        // Lookup view for data population
        TextView name = (TextView) convertView.findViewById(R.id.listview_cell_name);
        RatingBar review = (RatingBar) convertView.findViewById(R.id.listview_cell_review);
        TextView address = (TextView) convertView.findViewById(R.id.listview_cell_address);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.listview_cell_image);
        imageView.getLayoutParams().height = 300;
        imageView.getLayoutParams().width = 300;
        switch (restaurant.getType()) {
            case "Hamburgueseria":
                imageView.setImageResource(R.drawable.hamburguesa);
                break;
            case "Oriental":
                imageView.setImageResource(R.drawable.oriental);
                break;
            case "Mejicano":
                imageView.setImageResource(R.drawable.mejicano);
                break;
            case "Restaurante":
                imageView.setImageResource(R.drawable.restaurante);
                break;
            case "Nepali":
                imageView.setImageResource(R.drawable.nepali);
                break;
            case "Take Away":
                imageView.setImageResource(R.drawable.takeaway);
                break;
            case "Asador":
                imageView.setImageResource(R.drawable.asador);
                break;
            case "Frankfurt":
                imageView.setImageResource(R.drawable.hotdog);
                break;
        }

        // Populate the data into the template view using the data object
        name.setText(restaurant.getName());
        review.setRating(Float.parseFloat(restaurant.getReview()));
        address.setText(restaurant.getAddress());
        // Return the completed view to render on screen
        return convertView;
    }

    public void updateData(List<Restaurant> list){
        restaurants.clear();
        restaurants.addAll(list);
    }
}