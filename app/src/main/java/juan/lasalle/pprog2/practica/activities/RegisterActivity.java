package juan.lasalle.pprog2.practica.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.util.ArrayList;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.model.User;
import juan.lasalle.pprog2.practica.repositories.UsersRepo;
import juan.lasalle.pprog2.practica.repositories.impl.UsersDB;
import juan.lasalle.pprog2.practica.utils.DbBitmapUtility;

import static juan.lasalle.pprog2.practica.activities.MainActivity.SESSION_PREFERENCES;

/**
 * Created by juandelacruz on 20/4/17.
 */

public class RegisterActivity extends AppCompatActivity {
    private UsersRepo usersRepo;

    private static final int TAKE_PICTURE = 8;
    private static final int PICK_IMAGE = 9;
    public static final String TAG_MESSAGE = "message_registered";
    public static final int REGISTER_ACTIVITY = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usersRepo = new UsersDB(getApplicationContext());


        ImageView imageView = (ImageView) findViewById(R.id.draw_picture);
        imageView.getLayoutParams().height = 185;
        imageView.getLayoutParams().width = 185;
    }

    public void registerAccountOnClick(View view) {
        TextInputLayout tilSurName = (TextInputLayout) findViewById(R.id.surname_text_input_layout);
        TextInputLayout tilEmail = (TextInputLayout) findViewById(R.id.email_text_input_layout);
        TextInputLayout tilPasswordConf = (TextInputLayout) findViewById(R.id.confirm_text_input_layout);
        TextInputLayout tilPassword = (TextInputLayout) findViewById(R.id.password_text_input_layout);
        TextInputLayout tilName = (TextInputLayout) findViewById(R.id.name_text_input_layout);
        TextInputLayout tilDescription = (TextInputLayout) findViewById(R.id.description_text_input_layout);
        CheckBox acceptCheckBox = (CheckBox) findViewById(R.id.terms_cb);
        ImageView imageView = (ImageView) findViewById(R.id.draw_picture);

        TextInputEditText name = (TextInputEditText) findViewById(R.id.name_edittext);
        TextInputEditText surname = (TextInputEditText) findViewById(R.id.surname_edittext);
        TextInputEditText email = (TextInputEditText) findViewById(R.id.email_edittext);
        TextInputEditText password = (TextInputEditText) findViewById(R.id.password_edittext);
        TextInputEditText confirmPassword = (TextInputEditText) findViewById(R.id.confirmpassword_edittext);
        RadioGroup gender = (RadioGroup) findViewById(R.id.gender_radiogroup);
        TextInputEditText description = (TextInputEditText) findViewById(R.id.description_edittext);

        String selectedGender = ((RadioButton)findViewById(gender.getCheckedRadioButtonId())).getText().toString();
        //Si la aplicación está en español entonces sale en otro idioma al inglés
        //Lo pasamos al ingles para la base de datos
        if (selectedGender.equals("Sexo masculino")) {
            selectedGender = "Male";
        } else if (selectedGender.equals("Sexo femenino")) {
            selectedGender = "Female";
        }

        boolean error = false;


        //Comptador per saber quantes '@' hi ha en email
        int counter = 0;
        String stringEmail = email.getText().toString();
        for (int i = 0; i < stringEmail.length(); i++) {
            char valor = stringEmail.charAt(i);
            if (valor == '@') {
                counter = counter + 1;
            }
        }

        //Si el camp email esta buit hi haura error
        if (email.getText().toString().equals("")) {
            tilEmail.setError(getString(R.string.field_empty));
            error = true;
        }
        //Si no esta buit, si el comptador es diferent de 1 és que el email està mal posat
        else if (counter != 1) {
            tilEmail.setError(getString(R.string.email_error));
            error = true;
        }
        //Si cap de les anteriors, no hi ha cap error, es "neteja" el camp d'error
        else {
            tilEmail.setError(null);
        }

        boolean emptyPassword = false;
        //Es comprova si el camp de password esta buit
        if (password.getText().toString().equals("")) {
            tilPassword.setError(getString(R.string.field_empty));
            error = true;
            emptyPassword = true;
        } else {
            tilPassword.setError(null);
        }

        boolean emptyPasswordConf = false;
        //Es comprova si el camp de confirmació de password esta buit
        if (confirmPassword.getText().toString().equals("")) {
            tilPasswordConf.setError(getString(R.string.field_empty));
            error = true;
            emptyPasswordConf = true;
        } else {
            tilPasswordConf.setError(null);
        }

        //Si el camp de password i el de confirmació de password ambdues no estan buits
        if (!emptyPassword && !emptyPasswordConf) {
            //Llavors es comprova si ambdues password son diferents
            if (!(password.getText().toString().equals(confirmPassword.getText().toString()))) {
                //Son diferents, es llança error password does not match
                tilPasswordConf.setError(getString(R.string.password_match));
                error = true;
            }
            //Si hi ha alguna que esta buida es mira cual es per "netejar" el camp d'error
        } else if (!emptyPassword) {
            //Es renta camp d'error de Password
            tilPassword.setError(null);
        } else if (!emptyPasswordConf) {
            //Es renta camp d'error de Confirmació de password
            tilPasswordConf.setError(null);
        }

        if (name.getText().toString().equals("")) {
            tilName.setError(getString(R.string.field_empty));
            error = true;
        } else {
            tilName.setError(null);
        }

        if (surname.getText().toString().equals("")) {
            tilSurName.setError(getString(R.string.field_empty));
            error = true;
        } else {
            tilSurName.setError(null);
        }

        if (description.getText().toString().equals("")) {
            tilDescription.setError(getString(R.string.field_empty));
            error = true;
        } else {
            tilDescription.setError(null);
        }

        if (!acceptCheckBox.isChecked()) {
            error = true;
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.accept_terms_alert), Snackbar.LENGTH_LONG)
                    .show();
        }

        if (usersRepo.registeredUser(email.getText().toString())) {
            error = true;
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.registered_user_message), Snackbar.LENGTH_LONG)
                    .show();
        }


        if (!error) {
            imageView.buildDrawingCache();
            Bitmap bmap = imageView.getDrawingCache();
            byte[] imageBytes = DbBitmapUtility.getBytes(bmap);
            User user = new User(name.getText().toString(), surname.getText().toString(), email.getText().toString(), password.getText().toString(), selectedGender, description.getText().toString(), imageBytes);
            usersRepo.addUser(user);

            //Mensaje para mostrar en login como registrados correctamente
            String message = getString(R.string.successful_registered_alert);

            // Creamos un intent explícito que llame a la segunda activitidad.
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra(TAG_MESSAGE, message);

            // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
            // para poder reconocer el valor de retorno.
            startActivityForResult(intent, SearchActivity.SEARCH_ACTIVITY);

            SharedPreferences settings = getSharedPreferences(SESSION_PREFERENCES, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.clear();

            editor.putString("username", email.getText().toString());
            editor.commit();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void takeAPictureOnClick(View view) {
        // Creamos un intent implícito que llame a alguna aplicación capaz de tomar fotos.
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
        // para poder reconocer el valor de retorno.
        startActivityForResult(intent, TAKE_PICTURE);
    }

    public void chooseAPictureOnClick(View view) {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("palapa", Integer.toString(requestCode));
        // Filtramos resultado si es de hacer la foto
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                Bitmap image = (Bitmap) bundle.get("data");
                ImageView imageView = (ImageView) findViewById(R.id.draw_picture);
                if (imageView != null) {
                    imageView.setImageBitmap(image);
                }
            }
        }

        if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    // Log.d(TAG, String.valueOf(bitmap));

                    ImageView imageView = (ImageView) findViewById(R.id.draw_picture);
                    imageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}