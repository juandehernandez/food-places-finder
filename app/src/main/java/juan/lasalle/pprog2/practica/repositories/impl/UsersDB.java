package juan.lasalle.pprog2.practica.repositories.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.model.Comment;
import juan.lasalle.pprog2.practica.model.Restaurant;
import juan.lasalle.pprog2.practica.model.User;
import juan.lasalle.pprog2.practica.repositories.UsersRepo;
import juan.lasalle.pprog2.practica.utils.DatabaseHelper;

import static juan.lasalle.pprog2.practica.R.raw.db_creation;

/**
 * Created by juandelacruz on 18/4/17.
 */

public class UsersDB implements UsersRepo{
    // Contantes con los nombres de la tabla y de las columnas de la tabla.
    private static final String TABLE_USERS_NAME = "users";
    private static final String TABLE_SEARCHES_NAME = "searches";
    private static final String TABLE_FAVORITES_NAME = "favorites";
    private static final String TABLE_COMMENTS_NAME = "comments";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_SURNAME = "surname";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_GENDER = "gender";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_SEARCH = "search";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_LAT = "lat";
    private static final String COLUMN_LNG = "lng";
    private static final String COLUMN_TYPE = "type";
    private static final String COLUMN_REVIEW = "review";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_OPENING = "opening";
    private static final String COLUMN_CLOSING = "closing";
    private static final String COLUMN_ADDRESS = "address";
    private static final String COLUMN_COMMENT = "comment";

    private Context context;

    public UsersDB(Context context) {
        this.context = context;
    }

    @Override
    public void addUser(User p) {
        // Recuperamos una instancia del DatabaseHelper para poder acceder a la base de datos.
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Creamos los valores a añadir a la base de datos mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, p.getName());
        values.put(COLUMN_SURNAME, p.getSurname());
        values.put(COLUMN_EMAIL, p.getEmail());
        values.put(COLUMN_PASSWORD, p.getPassword());
        values.put(COLUMN_GENDER, p.getGender());
        values.put(COLUMN_DESCRIPTION, p.getDescription());
        values.put(COLUMN_IMAGE, p.getImage());
        // Obtenemos la base de datos en modo escritura e inserimos los valores en la Tabla
        // especificada. Los campos son: Nombre de la tabla, NUllColumnHack (sirve para añadir
        // una nueva fila con todos los valores de las columnas a NULL), y los valores a añadir.
        helper.getWritableDatabase().insert(TABLE_USERS_NAME, null, values);
        // El insert anterior equivale a la query SQL:
        // insert into TABLE_NAME (COLUMN_NAME, COLUMN_SURNAME...) values (p.getName(),
        // p.getSurname()...);
    }

    public void addSearch(String email, String search) {
        // Recuperamos una instancia del DatabaseHelper para poder acceder a la base de datos.
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Creamos los valores a añadir a la base de datos mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, email);
        values.put(COLUMN_SEARCH, search);

        // Obtenemos la base de datos en modo escritura e inserimos los valores en la Tabla
        // especificada. Los campos son: Nombre de la tabla, NUllColumnHack (sirve para añadir
        // una nueva fila con todos los valores de las columnas a NULL), y los valores a añadir.
        helper.getWritableDatabase().insert(TABLE_SEARCHES_NAME, null, values);
    }

    public void addFavorite(String email, Double lat, Double lng, String name, String type, String review, String description,
                            String address, String opening, String closing) {
        // Recuperamos una instancia del DatabaseHelper para poder acceder a la base de datos.
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Creamos los valores a añadir a la base de datos mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, email);
        values.put(COLUMN_LAT, lat);
        values.put(COLUMN_LNG, lng);
        values.put(COLUMN_NAME, name);
        values.put(COLUMN_TYPE, type);
        values.put(COLUMN_REVIEW, review);
        values.put(COLUMN_DESCRIPTION, description);
        values.put(COLUMN_ADDRESS, address);
        values.put(COLUMN_OPENING, opening);
        values.put(COLUMN_CLOSING, closing);

        // Obtenemos la base de datos en modo escritura e inserimos los valores en la Tabla
        // especificada. Los campos son: Nombre de la tabla, NUllColumnHack (sirve para añadir
        // una nueva fila con todos los valores de las columnas a NULL), y los valores a añadir.
        helper.getWritableDatabase().insert(TABLE_FAVORITES_NAME, null, values);
    }

    public void addComment(String email, Double lat, Double lng, String comment) {
        // Recuperamos una instancia del DatabaseHelper para poder acceder a la base de datos.
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Creamos los valores a añadir a la base de datos mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, email);
        values.put(COLUMN_LAT, lat);
        values.put(COLUMN_LNG, lng);
        values.put(COLUMN_COMMENT, comment);

        // Obtenemos la base de datos en modo escritura e inserimos los valores en la Tabla
        // especificada. Los campos son: Nombre de la tabla, NUllColumnHack (sirve para añadir
        // una nueva fila con todos los valores de las columnas a NULL), y los valores a añadir.
        helper.getWritableDatabase().insert(TABLE_COMMENTS_NAME, null, values);
    }

    public boolean existsFavorite(String email, Double lat, Double lng) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=? and " + COLUMN_LAT + "=? and " + COLUMN_LNG + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email, Double.toString(lat), Double.toString(lng)};

        SQLiteDatabase db = helper.getReadableDatabase();

        long count = DatabaseUtils.queryNumEntries(db, TABLE_FAVORITES_NAME, whereClause, whereArgs);

        return count > 0;
    }

    public boolean succesfulLogin(String email, String password) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=? and " + COLUMN_PASSWORD + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email, password};

        SQLiteDatabase db = helper.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_USERS_NAME, whereClause, whereArgs);

        return count > 0;
    }

    public boolean registeredUser(String email) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        SQLiteDatabase db = helper.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_USERS_NAME, whereClause, whereArgs);

        return count > 0;
    }

    public void updatePicture(String name, byte[] image) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        // Creamos los valores a sustituir mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_IMAGE, image);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {name};

        db.update(TABLE_USERS_NAME, values, whereClause, whereArgs);
    }

    public void updateName(String email, String newName) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        // Creamos los valores a sustituir mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, newName);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        db.update(TABLE_USERS_NAME, values, whereClause, whereArgs);
    }

    public void updateSurname(String email, String newSurname) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        // Creamos los valores a sustituir mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_SURNAME, newSurname);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        db.update(TABLE_USERS_NAME, values, whereClause, whereArgs);
    }

    public void updateDescription(String email, String newDescription) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        // Creamos los valores a sustituir mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_DESCRIPTION, newDescription);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        db.update(TABLE_USERS_NAME, values, whereClause, whereArgs);
    }

    public void updateGender(String email, String newGender) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        // Creamos los valores a sustituir mediante un conjunto <Clave, Valor>.
        ContentValues values = new ContentValues();
        values.put(COLUMN_GENDER, newGender);

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        db.update(TABLE_USERS_NAME, values, whereClause, whereArgs);
    }

    public byte[] getImage (String email) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Preparamos las columnas que queremos seleccionar. En este caso usaremos NULL para
        // indicar que queremos recuperar la fila entera.
        String[] selectColumns = null;

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        Cursor cursor = helper.getReadableDatabase().query(TABLE_USERS_NAME, selectColumns, whereClause, whereArgs, null, null, null, null);

        // Comprobamos que se nos haya devuelto un cursor válido.
        if (cursor != null) {
            // Movemos el cursor a la primera instancia. Si el cursor está vacío, devolverá falso.
            if (cursor.moveToFirst()) {
                return cursor.getBlob(cursor.getColumnIndex(COLUMN_IMAGE));
            }
            //Cerramos el cursor al terminar.
            cursor.close();
        }

        return null;
    }

    public User getUserProfile (String email) {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        // Preparamos las columnas que queremos seleccionar. En este caso usaremos NULL para
        // indicar que queremos recuperar la fila entera.
        String[] selectColumns = null;

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        Cursor cursor = helper.getReadableDatabase().query(TABLE_USERS_NAME, selectColumns, whereClause, whereArgs, null, null, null, null);

        // Comprobamos que se nos haya devuelto un cursor válido.
        if (cursor != null) {
            // Movemos el cursor a la primera instancia. Si el cursor está vacío, devolverá falso.
            if (cursor.moveToFirst()) {
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                String surname = cursor.getString(cursor.getColumnIndex(COLUMN_SURNAME));
                String gender = cursor.getString(cursor.getColumnIndex(COLUMN_GENDER));
                String description = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));

                User user = new User(name, surname, email, null, gender, description, null);

                return user;
            }
            //Cerramos el cursor al terminar.
            cursor.close();
        }

        return null;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> list = new ArrayList<>();

        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        //helper.getWritableDatabase().delete(TABLE_NAME, null, null);
        //helper.onCreate(helper.getReadableDatabase());
        // Preparamos las columnas que queremos seleccionar. En este caso usaremos NULL para
        // indicar que queremos recuperar la fila entera.
        String[] selectColumns = null;

        Cursor cursor = helper.getReadableDatabase().
                query(TABLE_USERS_NAME, selectColumns, null, null, null, null, null);
        // La query equivale en SQL a:
        // select * from TABLE_NAME;

        // Comprobamos que se nos haya devuelto un cursor válido.
        if (cursor != null) {
            // Movemos el cursor a la primera instancia. Si el cursor está vacío, devolverá falso.
            if (cursor.moveToFirst()) {
                do {
                    String userName = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                    String userSurname = cursor.getString(cursor.getColumnIndex(COLUMN_SURNAME));
                    String userEmail = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                    String userPassword = cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD));
                    String userGender = cursor.getString(cursor.getColumnIndex(COLUMN_GENDER));
                    String userDescription = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));
                    byte[] userImage = cursor.getBlob(cursor.getColumnIndex(COLUMN_IMAGE));

                    User p = new User(userName, userSurname, userEmail, userPassword, userGender, userDescription, userImage);
                    list.add(p);
                } while (cursor.moveToNext());
            }
            //Cerramos el cursor al terminar.
            cursor.close();
        }

        return list;
    }

    public List<String> getSearchesFromUser(String email) {
        List<String> list = new ArrayList<>();

        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        //helper.getWritableDatabase().delete(TABLE_NAME, null, null);
        //helper.onCreate(helper.getReadableDatabase());
        // Preparamos las columnas que queremos seleccionar. En este caso usaremos dos columnas para
        // indicar que queremos recuperar solo las filas con esos valores.
        String[] selectColumns = {COLUMN_SEARCH, COLUMN_ID};

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        Cursor cursor = helper.getReadableDatabase().query(TABLE_SEARCHES_NAME, selectColumns, whereClause, whereArgs, null, null, "_id DESC", "7");
        // La query equivale en SQL a:
        // select * from TABLE_NAME;

        // Comprobamos que se nos haya devuelto un cursor válido.
        if (cursor != null) {
            // Movemos el cursor a la primera instancia. Si el cursor está vacío, devolverá falso.
            if (cursor.moveToFirst()) {
                do {
                    String search = cursor.getString(cursor.getColumnIndex(COLUMN_SEARCH));

                    list.add(search);
                } while (cursor.moveToNext());
            }
            //Cerramos el cursor al terminar.
            cursor.close();
        }

        return list;
    }

    public List<Restaurant> getFavoritesFromUser(String email) {
        List<Restaurant> list = new ArrayList<>();

        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        //helper.getWritableDatabase().delete(TABLE_NAME, null, null);
        //helper.onCreate(helper.getReadableDatabase());
        // Preparamos las columnas que queremos seleccionar. En este caso usaremos NULL para
        // indicar que queremos recuperar la fila entera.
        String[] selectColumns = null;

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_EMAIL + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {email};

        Cursor cursor = helper.getReadableDatabase().query(TABLE_FAVORITES_NAME, selectColumns, whereClause, whereArgs, null, null, "_id DESC", "7");
        // La query equivale en SQL a:
        // select * from TABLE_NAME;

        // Comprobamos que se nos haya devuelto un cursor válido.
        if (cursor != null) {
            // Movemos el cursor a la primera instancia. Si el cursor está vacío, devolverá falso.
            if (cursor.moveToFirst()) {
                do {
                    Double lat = cursor.getDouble(cursor.getColumnIndex(COLUMN_LAT));
                    Double lng = cursor.getDouble(cursor.getColumnIndex(COLUMN_LNG));
                    String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                    String type = cursor.getString(cursor.getColumnIndex(COLUMN_TYPE));
                    String review = cursor.getString(cursor.getColumnIndex(COLUMN_REVIEW));
                    String description = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));
                    String address = cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS));
                    String opening = cursor.getString(cursor.getColumnIndex(COLUMN_OPENING));
                    String closing = cursor.getString(cursor.getColumnIndex(COLUMN_CLOSING));

                    Restaurant restaurant = new Restaurant(new LatLng(lat, lng), name, type, review, description, address, opening, closing);

                    list.add(restaurant);
                } while (cursor.moveToNext());
            }
            //Cerramos el cursor al terminar.
            cursor.close();
        }

        if (list.isEmpty()) {
            Log.d("list null", "list null");
        }
        return list;
    }

    public List<Comment> getCommentsFromRestaurant(LatLng restaurant) {
        List<Comment> list = new ArrayList<>();

        DatabaseHelper helper = DatabaseHelper.getInstance(context);

        //helper.getWritableDatabase().delete(TABLE_NAME, null, null);
        //helper.onCreate(helper.getReadableDatabase());
        // Preparamos las columnas que queremos seleccionar. En este caso usaremos NULL para
        // indicar que queremos recuperar la fila entera.
        String[] selectColumns = {COLUMN_EMAIL, COLUMN_ID, COLUMN_COMMENT};

        // Preparamos la cláusula del where. Su formato es: "<nombre columna> = ?" donde ? se
        // sustituirá por el valor añadido en los argumentos.
        String whereClause = COLUMN_LAT + "=? and " + COLUMN_LNG + "=?";
        // Preparamos los argumentos a sustituir por los '?' de la cláusula.
        String[] whereArgs = {Double.toString(restaurant.latitude), Double.toString(restaurant.longitude)};

        Cursor cursor = helper.getReadableDatabase().query(TABLE_COMMENTS_NAME, selectColumns, whereClause, whereArgs, null, null, "_id DESC", null);
        // La query equivale en SQL a:
        // select * from TABLE_NAME;

        // Comprobamos que se nos haya devuelto un cursor válido.
        if (cursor != null) {
            // Movemos el cursor a la primera instancia. Si el cursor está vacío, devolverá falso.
            if (cursor.moveToFirst()) {
                do {
                    String user = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                    String commentText = cursor.getString(cursor.getColumnIndex(COLUMN_COMMENT));

                    Comment comment = new Comment(user, commentText);

                    list.add(comment);
                } while (cursor.moveToNext());
            }
            //Cerramos el cursor al terminar.
            cursor.close();
        }

        return list;
    }

    public void deleteRows() {
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        db.execSQL("delete from "+ TABLE_USERS_NAME);
    }
}
