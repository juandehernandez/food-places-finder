package juan.lasalle.pprog2.practica.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.activities.DescriptionActivity;
import juan.lasalle.pprog2.practica.adapters.RestaurantListViewAdapter;
import juan.lasalle.pprog2.practica.model.Restaurant;

/**
 * Created by juandelacruz on 11/5/17.
 */

public class OpenListFragment extends android.support.v4.app.Fragment {
    private RestaurantListViewAdapter adapter;
    private List<Restaurant> restaurantListOpen;
    private String[] spinnerTypes;

    private static final int DESCRIPTION_ACTIVITY = 5;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_listview, container, false);

        // Recuperamos el componente gráfico para poder asignarle un adapter.
        ListView listView = (ListView) view.findViewById(R.id.listview);

        restaurantListOpen = new ArrayList<>();

        // Creamos el adapter
        adapter = new RestaurantListViewAdapter(getActivity(), restaurantListOpen);

        // Vinculamos el adapter a la ListView.
        listView.setAdapter(adapter);

        // Vinculamos el adapter (que implementa un OnItemClickListener) como Listener de cada
        // uno de los items de la ListView.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Creamos un intent explícito que llame a la segunda activitidad.
                Intent intent = new Intent(getActivity(), DescriptionActivity.class);
                intent.putExtra("name", restaurantListOpen.get(position).getName());
                intent.putExtra("review", restaurantListOpen.get(position).getReview());
                intent.putExtra("description", restaurantListOpen.get(position).getDescription());
                intent.putExtra("type", restaurantListOpen.get(position).getType());
                intent.putExtra("coords", restaurantListOpen.get(position).getCoords());
                intent.putExtra("address", restaurantListOpen.get(position).getAddress());
                intent.putExtra("opening", restaurantListOpen.get(position).getOpening());
                intent.putExtra("closing", restaurantListOpen.get(position).getClosing());
                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, DESCRIPTION_ACTIVITY);
            }
        });

        listView.setFastScrollAlwaysVisible(true);

        return view;
    }

    public void dataChanged(List<Restaurant> restaurantList) {
        if (!restaurantList.isEmpty()) {
            //Guardamos la lista generada en la búsqueda, si está vacía se pone en el atributo privado del fragmento
            //Solo se pondrá la primera vez, de esta manera podemos tener referencia a la lista principal al cambiar
            // el filtro del spinner
            if (this.restaurantListOpen.isEmpty()) {
                this.restaurantListOpen = restaurantList;
            }
            adapter.clear();
            adapter.updateData(restaurantList);
            adapter.notifyDataSetChanged();
        } else {
            TextView emptyListTextView = (TextView) getView().findViewById(R.id.empty_list_textview);
            emptyListTextView.setText(getString(R.string.empty_list));
            emptyListTextView.setVisibility(View.VISIBLE);
        }
    }

    public void setSpinnerTypes(String[] spinnerTypes) {
        this.spinnerTypes = spinnerTypes;
    }

    public void tabSelected() {
        setSpinnerAdapter();

        final Spinner spinner = (Spinner) getActivity().findViewById(R.id.restaurant_types_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    if (spinner.getItemAtPosition(position).toString().equals(getString(R.string.spinner_all))) {
                        dataChanged(restaurantListOpen);
                    } else {
                        List<Restaurant> restaurantListType = new ArrayList<>();
                        for (int i = 0; i < restaurantListOpen.size(); i++) {
                            if (restaurantListOpen.get(i).getType().equals(spinner.getItemAtPosition(position).toString())) {
                                restaurantListType.add(restaurantListOpen.get(i));
                            }
                        }
                        dataChanged(restaurantListType);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSpinnerAdapter() {
        //Creamos adapter de spinner con los valores i le asignamos éste adapter
        final Spinner spinner = (Spinner) getActivity().findViewById(R.id.restaurant_types_spinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(),
                android.R.layout.simple_spinner_item, spinnerTypes) {
            //Hacemos override del método así podemos poner un texto por defecto en el spinner (array primera casilla)
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                View v = null;

                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    tv.setVisibility(View.GONE);
                    v = tv;
                }
                else {
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);
                }

                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
    }
}
