package juan.lasalle.pprog2.practica.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

import java.util.List;

import juan.lasalle.pprog2.practica.R;
import juan.lasalle.pprog2.practica.adapters.RestaurantListViewAdapter;
import juan.lasalle.pprog2.practica.adapters.TabAdapter;
import juan.lasalle.pprog2.practica.fragments.AllListFragment;
import juan.lasalle.pprog2.practica.fragments.OpenListFragment;
import juan.lasalle.pprog2.practica.model.Restaurant;
import juan.lasalle.pprog2.practica.repositories.RestaurantWebServiceRepo;
import juan.lasalle.pprog2.practica.repositories.impl.RestaurantWebService;
import juan.lasalle.pprog2.practica.utils.OpenRestaurantsUtility;

/**
 * Created by juandelacruz on 11/5/17.
 */

public class ResultsActivity extends AppCompatActivity{
    private AllListFragment allListFragment;
    private OpenListFragment openListFragment;
    private RestaurantWebServiceRepo restaurantWebServiceRepo;

    public static final int RESULTS_ACTIVITY = 4;
    public static final String inputFormat = "HH:mm";
    public static final String TAG_MESSAGE = "message_empty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        //Creamos nuevos objetos para los fragmentos de las listas de restaurantes
        //así les pasamos la lista de restaurantes con su método dataChanged
        allListFragment = new AllListFragment();
        openListFragment = new OpenListFragment();

        //Generem repositori que s'encarrega de obtenir les dades del web service
        restaurantWebServiceRepo = new RestaurantWebService();

        //Detectar el cambio de tab, de esta manera modificamos spinner de los filtros
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().toString().equals(getString(R.string.all_tab))) {
                    allListFragment.tabSelected();
                } else if (tab.getText().toString().equals(getString(R.string.only_open_tab))) {
                    openListFragment.tabSelected();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if ((getIntent().getExtras().get(SearchActivity.TAG_METHOD)).equals(SearchActivity.TAG_CRITERIA)) {
            new AsyncRequest().execute((String) getIntent().getExtras().get(SearchActivity.TAG_METHOD),
                    (String) getIntent().getExtras().get(SearchActivity.TAG_CRITERIA));
        } else if ((getIntent().getExtras().get(SearchActivity.TAG_METHOD)).equals(SearchActivity.TAG_GEOLOCATION)){
            Location location = (Location) getIntent().getExtras().get(SearchActivity.TAG_GEOLOCATION);
            int distance = (int) getIntent().getExtras().get(SearchActivity.TAG_DISTANCE);
            new AsyncRequest().execute((String) getIntent().getExtras().get(SearchActivity.TAG_METHOD),
                    Double.toString(location.getLatitude()), Double.toString(location.getLongitude()), Integer.toString(distance));
        }
    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
    }

    private void createTabs() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);

        ArrayList<TabAdapter.TabEntry> entries = new ArrayList<>();
        entries.add(new TabAdapter.TabEntry(allListFragment, getString(R.string.all_tab)));
        entries.add(new TabAdapter.TabEntry(openListFragment, getString(R.string.only_open_tab)));

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager(), entries);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private class AsyncRequest extends AsyncTask<String, Void, List<Restaurant>> {

        private Context context;
        private ProgressDialog progressDialog;

        protected AsyncRequest() {
            this.context = ResultsActivity.this;
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage(getString(R.string.wait_message));
            progressDialog.show();
        }

        @Override
        protected List<Restaurant> doInBackground(String... params) {
            switch (params[0]) {
                case SearchActivity.TAG_CRITERIA:
                    return restaurantWebServiceRepo.search(params[1]);
                case SearchActivity.TAG_GEOLOCATION:
                    return restaurantWebServiceRepo.searchGeolocation(params[1], params[2], params[3]);
                case SearchActivity.TAG_FAVORITES:
                    //TODO
                    return null;
            }

            return null;
        }

        @Override
        protected void onPostExecute(final List<Restaurant> list) {
            super.onPostExecute(list);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            createToolbar();
            createTabs();

            if (list.isEmpty()) {
                //Si la lista está vacía volvemos a la pantalla de búsqueda con un mensaje

                //Mensaje para mostrar en SearchActivity
                String message = getString(R.string.empty_search_message);

                // Creamos un intent explícito que llame a la actividad de búsqueda.
                Intent intent = new Intent(getApplication(), SearchActivity.class);
                intent.putExtra(TAG_MESSAGE, message);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, SearchActivity.SEARCH_ACTIVITY);
            } else {
                List<Restaurant> openRestaurants = OpenRestaurantsUtility.getOpenRestaurants(list);


                //Ahora le pasaremos a cada fragmento los tipos de lugares de comida (Mejicano, Asador...)
                //para filtros del spinner

                //Generamos arraylist para obtener los tipos diferentes de restaurantes (total de restaurantes)
                List<String> restaurantTypesAll = new ArrayList<>();
                List<String> restaurantTypesOpen = new ArrayList<>();
                restaurantTypesAll.add(0, getString(R.string.default_text_spinner));
                restaurantTypesOpen.add(0, getString(R.string.default_text_spinner));
                //Bucle todos los restaurantes
                for (int i = 0; i < list.size(); i++) {
                    if (!restaurantTypesAll.contains(list.get(i).getType())) {
                        restaurantTypesAll.add(list.get(i).getType());
                    }
                }
                //Bucle restaurantes abiertos
                for (int i = 0; i < openRestaurants.size(); i++) {
                    if (!restaurantTypesOpen.contains(openRestaurants.get(i).getType())) {
                        restaurantTypesOpen.add(openRestaurants.get(i).getType());
                    }
                }

                //Le ponemos también el tipo TODOS
                // así se puede volver a mostrar todos los restaurantes para el spinner
                restaurantTypesAll.add(getString(R.string.spinner_all));
                restaurantTypesOpen.add(getString(R.string.spinner_all));

                //En el arrayadapter se necesita un array de String por eso convertimos el arraylist a array
                String[] spinnerTypesAll = new String[restaurantTypesAll.size()];
                spinnerTypesAll = restaurantTypesAll.toArray(spinnerTypesAll);

                //y ahora el de restaurantes abiertos
                String[] spinnerTypesOpen = new String[restaurantTypesOpen.size()];
                spinnerTypesOpen = restaurantTypesOpen.toArray(spinnerTypesOpen);

                allListFragment.setSpinnerTypes(spinnerTypesAll);
                openListFragment.setSpinnerTypes(spinnerTypesOpen);


                //Finalmente pasamos las listas de restaurantes a cada fragmento
                allListFragment.dataChanged(list);
                openListFragment.dataChanged(openRestaurants);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Este metodo se llamara una vez durante la creacion de la Activity
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Este metodo se llamara cada vez que se pulse en algun Item de la Action Bar
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_profile_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, ProfileActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, ProfileActivity.PROFILE_ACTIVITY);
                break;
            case R.id.menu_favorites_action_button:
                // Creamos un intent explícito que llame a la segunda activitidad.
                intent = new Intent(this, FavoritesActivity.class);

                // Lanzamos el intent para que nos devuelva un resultado y configuramos el requestCode
                // para poder reconocer el valor de retorno.
                startActivityForResult(intent, FavoritesActivity.FAVORITES_ACTIVITY);
            default:

        }
        return true;
    }
}
